### NewsFeed Microservice
 NewsFeed is a SpringBoot-based microservice that provides a simple interface for managing news and photos. This microservice provides two endpoints through which the User can add news and retrieve news based on a photo.


## Getting Started :
Clone this repository to your local machine:
git clone https://gitlab.com/mechergui.aymen/follow.git

To use this microservice, you need to have Java 11 or later installed on your machine. Clone the repository and navigate to the project directory. To run the microservice, use the following command:

./mvnw spring-boot:run


## API Endpoints
The microservice provides the following two endpoints for managing news:

## Add News
POST /news

{
  "title": "News Title",
  "description": "News Description",
  "photoUrl": "http://localhost:9002/api/v1/news"
}


This endpoint accepts a JSON payload with the following fields:

title - the title of the news
description - a brief description of the news
photoUrl - the URL of the photo associated with the news
The endpoint returns the ID of the newly created news as a Long.


## Get News
GET /news

{
  "photoUrl": "http://localhost:9002/api/v1/news"
}



This endpoint accepts a JSON payload with the following field:

photoUrl - the URL of the photo to retrieve news for
The endpoint returns a Set of News objects associated with the photo URL.


## INewsServices Interface
This microservice uses the INewsServices interface, which provides the following methods:

public interface INewsServices {
  Long ajouterNews(News ns);
  Set<News> getNews(Photo ph);
}

ajouterNews - adds a news object to the microservice
getNews - retrieves a set of news objects associated with a photo

## Technologies Used
Java 11
SpringBoot
Maven

## Author
This microservice was developed by [Alaa Eddine Nouili]



