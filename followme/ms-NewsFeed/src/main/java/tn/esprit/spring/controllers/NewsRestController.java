package tn.esprit.spring.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import tn.esprit.spring.entities.News;
import tn.esprit.spring.repositories.INewsRepository;


@RestController
@RequestMapping("/api/v1/news")
public class NewsRestController { 
	
	@Autowired
	INewsRepository newsrepository;

	
	@PostMapping()
	public void addNews(@RequestBody News news) {
		newsrepository.save(news);
	}	
	
	
//	@PostMapping("/addnews")
//	public ResponseEntity<News> addNews(@RequestBody News news) {
//	    News newsId = newsrepository.save(news);
//	    return ResponseEntity.ok(newsId);
//	}

	@GetMapping()
	public List<News> getallNews(){
		return newsrepository.findAll();
		}
	
	@GetMapping("/{idUser}")
	public Optional<News> getNews(@PathVariable Long idUser){
		return newsrepository.findById(idUser);
		
	}
	
	
	
	
	
	
	
	/*
	 * @PostMapping("/addnews")
	 * 
	 * @ResponseBody public Long addNews2(@RequestBody News news){
	 * 
	 * return newsservices.ajouterNews(news); }
	 */
	 
	
	
	
	
}
