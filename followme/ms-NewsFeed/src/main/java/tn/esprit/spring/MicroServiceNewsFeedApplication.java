package tn.esprit.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;

@SpringBootApplication
public class MicroServiceNewsFeedApplication  {
	
		public static void main(String[] args) {
		SpringApplication.run(MicroServiceNewsFeedApplication.class, args);
	}

	/*
	 * @Override public void run(String... args) throws Exception {
	 * 
	 * //System.out.println("------------" + MongoTemplate);
	 * 
	 * // TODO Auto-generated method stub
	 * 
	 * }
	 */

}
