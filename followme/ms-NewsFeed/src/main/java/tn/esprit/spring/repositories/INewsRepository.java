package tn.esprit.spring.repositories;




import org.springframework.data.mongodb.repository.MongoRepository;



import tn.esprit.spring.entities.News;

public interface INewsRepository extends MongoRepository<News, Long>{
	

}
