package com.posti.of.authentication.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

	public static final String PARAM_CLIENT_ID = "client_id";
	public static final String PARAM_CLIENT_SECRET = "client_secret";
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_GRANT_TYPE = "grant_type";
	public static final String GRANT_TYPE_CREDENTIALS = "client_credentials";
    public static final String Param_TOKEN="token";

	public static final String CREATE_USER="CREATE_USER";
	public static final String UPDATE_USER="UPDATE_USER";
	public static final String DELETE_USER="DELETE_USER";

}
