package com.posti.of.authentication.exception;

import com.posti.of.authentication.domain.beans.Error;
import com.posti.of.authentication.domain.beans.ErrorDetails;
import com.posti.of.authentication.domain.enums.InternalServerErrorCodes;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;

public class InternalServerErrorException extends WebApplicationException {

    public InternalServerErrorException(InternalServerErrorCodes errorCode, List<ErrorDetails> details){
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(Error.builder().code(errorCode.getCode()).message(errorCode.getMessage())
                        .details(details)
                        .build()).build());
    }
}
