package com.posti.of.authentication.domain.rest;
import com.posti.of.authentication.constant.Constants;
import com.posti.of.authentication.domain.beans.*;
import com.posti.of.authentication.domain.enums.ErrorMessage;
import com.posti.of.authentication.domain.enums.GrantTypeEnum;
import com.posti.of.authentication.exception.RequiredException;
import com.posti.of.authentication.service.AuthenticationService;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.smallrye.mutiny.Uni;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RegisterForReflection
@Path("/api/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthenticationQueryResource {

	private final Logger log = LoggerFactory.getLogger(AuthenticationQueryResource.class);

	@Inject
	AuthenticationService authService;

	@POST
	@Path("/login")
	public Uni<ResponseAuth> getToken(@Context  HttpHeaders headers, RequestAuth requestAuth) {
		log.info("request to get token : {}", requestAuth);

		//validateEntity(requestAuth);

		return authService.checkAndGetAccessToken(headers,requestAuth);


	}

/*	public void validateEntity(RequestAuth requestAuth) {

		List<ErrorDetails> details = new ArrayList<>();
		if (requestAuth.getGrantType() == null) {
			details.add(new ErrorDetails(Constants.FIELD_GRANT_TYPE, Constants.IS_REQUIRED_FIELD ));
		} else {
			if (requestAuth.getGrantType().equals(GrantTypeEnum.ACCESS_TOKEN)) {
				if(!(requestAuth.getUsername() != null && !requestAuth.getUsername().isEmpty()))
					details.add(new ErrorDetails(Constants.FIELD_USERNAME ,  Constants.IS_REQUIRED_FIELD));

			}
		}
		if (!details.isEmpty())
			throw new RequiredException(ErrorMessage.REQUIRED_FIELD, details);
	}*/

	@POST
	@Path("/loginAdmin")
	public Uni<ResponseAuth> getAdminToken() {
		log.info("request to get Admin token : {}");

		return authService.getAdminAccessToken();


	}
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/verifyToken")
	public Uni<ResponseVerification> verifyToken(RequestVerify params) {
		log.info("request to verify  token : {}",params);

		return authService.validateToken(params);


	}
	@GET
	@Path("/test")
	public Response hello()
	{
		return  Response.status(200).entity("test").build();
	}

}
