package com.posti.of.authentication.exception;

import com.posti.of.authentication.domain.beans.Error;
import com.posti.of.authentication.domain.beans.ErrorDetails;
import com.posti.of.authentication.domain.enums.ErrorMessage;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;


import java.util.List;


public class RequiredException extends WebApplicationException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RequiredException(ErrorMessage errorCode,List<ErrorDetails> details){
        super(Response.status(Response.Status.BAD_REQUEST)
                .entity(Error.builder().code(errorCode.getCode()).message(errorCode.getMessage())
                        .details(details)
                        .build()).build());
    }
}
