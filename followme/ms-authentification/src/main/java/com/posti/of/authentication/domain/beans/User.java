package com.posti.of.authentication.domain.beans;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {
	private String id;
	private String username;
	private String password;
	private String firstName;
	private String lastname;
}
