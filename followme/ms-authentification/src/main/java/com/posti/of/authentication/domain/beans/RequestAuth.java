package com.posti.of.authentication.domain.beans;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import com.posti.of.authentication.domain.enums.GrantTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "requestAuth")
public class RequestAuth {

	
	private String username;
    private String password;

    private GrantTypeEnum  grantType;

	
	
	
}
