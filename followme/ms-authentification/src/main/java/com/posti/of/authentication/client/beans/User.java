package com.posti.of.authentication.client.beans;

import java.util.HashMap;
import java.util.List;

import io.quarkus.security.credential.Credential;
import io.quarkus.security.credential.PasswordCredential;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.keycloak.representations.idm.CredentialRepresentation;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "user")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {


	private String id;
	private String username;
	private String firstName;
	private String lastName;
	private Boolean enabled;
	private CredentialRepresentation password;
	private List<String> roles;

}
