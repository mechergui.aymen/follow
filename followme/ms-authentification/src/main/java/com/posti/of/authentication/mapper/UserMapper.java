package com.posti.of.authentication.mapper;

import com.posti.of.authentication.constant.Constants;
import com.posti.of.authentication.domain.beans.User;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import java.util.ArrayList;
import java.util.List;

@Mapper(
        componentModel = "cdi"
)
@ApplicationScoped
public interface UserMapper {
    @Produces
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "enabled", constant = "true")
    @Mapping(target = "password", expression = "java(getPassword(user))")
    @Mapping(target = "roles", expression = "java(getRoles())")
    com.posti.of.authentication.client.beans.User map(User user);

    default List<String> getRoles() {
        List<String> attributes = new ArrayList<>();
        attributes.add("admin");
        return attributes;
    }

    default CredentialRepresentation getPassword(User u) {
        CredentialRepresentation credentialRepresentation= new CredentialRepresentation();
       credentialRepresentation.setType(Constants.PARAM_PASSWORD);
       credentialRepresentation.setValue(u.getPassword());

        return credentialRepresentation;
    }
}
