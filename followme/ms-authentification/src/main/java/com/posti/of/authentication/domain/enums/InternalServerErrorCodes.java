package com.posti.of.authentication.domain.enums;

public enum InternalServerErrorCodes {
    USER_CREDENTIAL_EXCEPTION("4001", "USER Credential is not correct. "),
    INTERNAL_SERVER_ERROR("5000", "Internal server error (connectivity issues, maintenance, etc.). "),
    ENTITY_NOT_FOUND("5001", "Entity Not found."),
    RELATED_ENTITY_NOT_FOUND("5002", "Related Entity Not found."),
    CONSTRAINT_FIELDS_REQUIRED("5003", "Constraint fields required."),
    PREREQUISITE_CONDITION("5004", "Prerequisite Condition are not accepted to validate an action."),
    EXTERNAL_DEPENDENCIES("5005", "Delete External  Dependencies."),
    INNER_DEPENDENCIES("5006", "Delete Internal Dependencies."),
    PUBLISHED_CATEGORY("5007", "Delete Pre-requisite"),
    UNKNOWN_ERROR("5099", "Unknown error."),
    SUBJECT_TOKEN_EXCEPTION("5008","Cannot get subject Token from Keycloak!"),
    ACCESS_TOKEN_EXCEPTION("5008","Cannot get access Token for this username!"), 
    REFRESH_TOKEN_EXCEPTION("5008","Invalid Refresh Token !"),

    ADMIN_TOKEN_EXCEPTION("5008","Admin config  is invalid!"),
    ACCESS_TOKEN_INVALID("5009","TOKEN IS INVADE" );

    private final String code;
    private final String message;

    private InternalServerErrorCodes(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + message;
    }
}
