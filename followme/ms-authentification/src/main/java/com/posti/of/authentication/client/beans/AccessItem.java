package com.posti.of.authentication.client.beans;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "accessItem")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccessItem {

	private Boolean manageGroupMembership;
	private Boolean view;
	private Boolean mapRoles;
	private Boolean impersonate;
	private Boolean manage;

}
