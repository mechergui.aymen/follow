package com.posti.of.authentication.client;


import io.smallrye.mutiny.Uni;

import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.posti.of.authentication.client.beans.User;
import com.posti.of.authentication.domain.beans.ResponseAuth;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RegisterRestClient
@RegisterClientHeaders
public interface KeycloakClient {

    @POST
    @Path("/realms/{realmName}/protocol/openid-connect/token")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    Uni<ResponseAuth> getApplicationClientToken(@PathParam("realmName") String realmName , String params);

    @POST
    @Path("/realms/{realmName}/protocol/openid-connect/token")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    Uni<ResponseAuth> getUserToken(@PathParam("realmName") String realmName , String params);
    @GET
    @Path("/admin/realms/master/users")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Uni<List<User>> getUserByUserName(@HeaderParam("Authorization") String authorization, @QueryParam("username") String username);


    @POST
    @Path("/admin/realms/master/users")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Uni<Response> createUser(@HeaderParam("Authorization") String authorization, User user);


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/realms/master/protocol/openid-connect/token/introspect")
    public Uni<Response> verifyUserToken( String params);

    
}
