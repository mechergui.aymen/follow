package com.posti.of.authentication.client;

import com.posti.of.authentication.constant.Constants;
import com.posti.of.authentication.domain.beans.User;
import com.posti.of.authentication.domain.enums.Topic;
import com.posti.of.authentication.mapper.UserMapper;
import com.posti.of.authentication.service.AuthenticationService;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class UserConsumer {
    private final Logger log = LoggerFactory.getLogger(UserConsumer.class);

    @Inject
    AuthenticationService service;

    @Inject
    UserMapper userMapper;
    @Incoming("CREATE_USER")
    public void createUser(User user)
    {
        log.info("Receive New Data From KAFKA {}",user);
        log.info("Receive New Data From MAPSTRUCT {}",userMapper.map(user));
        service.getAdminAccessToken()
                .call(responseAuth -> service.createUser(responseAuth.getAccessToken(), user))
                .map(response -> {log.info("User ADDED Successfully ");return Uni.createFrom().voidItem();});

    }
}
