package com.posti.of.authentication.domain.enums;

public enum ErrorMessage {
    INVALID_REQUEST("4000", "Invalid request format (JSON request could not be parsed)."),
    REQUIRED_FIELD("4001", "Required field."),
    INVALID_FIELD_VALUE("4002", "Invalid field value."),
    INVALID_FIELD_TYPE("4003", "Invalid field type.");

    private final String code;
    private final String message;

    private ErrorMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + message;
    }
}
