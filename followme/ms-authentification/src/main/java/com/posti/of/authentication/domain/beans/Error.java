package com.posti.of.authentication.domain.beans;

import lombok.*;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@Data@NoArgsConstructor@AllArgsConstructor@Builder@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {
    private String code;
    private String message;
    private List<ErrorDetails> details;
}
