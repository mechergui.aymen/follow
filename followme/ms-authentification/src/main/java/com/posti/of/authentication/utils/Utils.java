package com.posti.of.authentication.utils;

import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class Utils {

    public static Map<String, String> convert(String formEncodedString) {
        return Arrays.stream(formEncodedString.split("&"))
                .map(pair -> pair.split("="))
                .collect(Collectors.toMap(pair -> URLDecoder.decode(pair[0]), pair -> URLDecoder.decode(pair[1])));
    }
}
