package com.posti.of.authentication.service;

import java.net.URLEncoder;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


import com.posti.of.authentication.client.KeycloakClient;
import com.posti.of.authentication.client.beans.User;
import com.posti.of.authentication.constant.Constants;
import com.posti.of.authentication.domain.beans.RequestAuth;
import com.posti.of.authentication.domain.beans.RequestVerify;
import com.posti.of.authentication.domain.beans.ResponseAuth;
import com.posti.of.authentication.domain.beans.ResponseVerification;
import com.posti.of.authentication.domain.enums.InternalServerErrorCodes;
import com.posti.of.authentication.exception.InternalServerErrorException;
import com.posti.of.authentication.mapper.UserMapper;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.annotations.RegisterForReflection;
import io.smallrye.mutiny.Uni;

@ApplicationScoped
@RegisterForReflection
public class AuthenticationService {
@Inject
	@RestClient
KeycloakClient keycloakClient;
	private final Logger log = LoggerFactory.getLogger(AuthenticationService.class);

	@ConfigProperty(name = "com.posti.of.client.KeycloakClient.client-id")
	public String clientId;

	@ConfigProperty(name = "com.posti.of.client.KeycloakClient.client-secret")
	public String clientSecret;


   @Inject
   UserMapper userMapper;

	@ConfigProperty(name = "com.posti.of.client.KeycloakClient.realm.name")
	public String realmName;


	public Uni<ResponseAuth> getAdminAccessToken() {
		Map<String, String> map = new HashMap<>();
		map.put(Constants.PARAM_CLIENT_ID, clientId);
		map.put(Constants.PARAM_CLIENT_SECRET, clientSecret);
		map.put(Constants.PARAM_GRANT_TYPE, Constants.GRANT_TYPE_CREDENTIALS);
		map.put("realm", realmName);

		String params = map.entrySet().stream()
				.map(entry -> Stream
						.of(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8),
								URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8))
						.collect(Collectors.joining("=")))
				.collect(Collectors.joining("&"));
		log.info(this.realmName);
		return keycloakClient.getApplicationClientToken(realmName, params);

	}

	public Uni<Response> createUser(String authentification, com.posti.of.authentication.domain.beans.User user) {

		log.info("create User {}",user);
		return keycloakClient.createUser(authentification, userMapper.map(user));

	}

	public Uni<Boolean> isExistedUser(String token, String username) {

		//	headers.add(HttpHeaders.AUTHORIZATION, token);
		//	headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
		return keycloakClient.getUserByUserName( token, username).onItem().transform(users -> {

			log.info(" check user : {}", users);
			if (filtredUserByUsername(users, username) != null) {
				return true;
			} else {
				return false;
			}
		});

	}

	private User filtredUserByUsername(List<User> users, String username) {
		Optional<User> userFounded = users.stream().filter(user -> user.getUsername().equalsIgnoreCase(username))
				.findFirst();
		return userFounded.isPresent() ? userFounded.get() : null;
	}

	public Uni<ResponseAuth> getUserAccessToken(String acessTokenToKeycloak, RequestAuth requestAuth) {
		Map<String, String> map = new HashMap<>();
		map.put(Constants.PARAM_CLIENT_ID, clientId);
		map.put(Constants.PARAM_CLIENT_SECRET, clientSecret);
		map.put(Constants.PARAM_USERNAME, requestAuth.getUsername());
		map.put(Constants.PARAM_PASSWORD, requestAuth.getPassword());
		map.put(Constants.PARAM_GRANT_TYPE, Constants.PARAM_PASSWORD);



		String params = map
				.entrySet().stream()
				.map(entry -> Stream
						.of(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8),
								URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8))
						.collect(Collectors.joining("=")))
				.collect(Collectors.joining("&"));

		return keycloakClient.getUserToken(realmName ,params);

	}
	public Uni<Response> verifyToken(RequestVerify requestVerify) {
		Map<String, String> map = new HashMap<>();
		map.put(Constants.PARAM_CLIENT_ID, clientId);
		map.put(Constants.PARAM_CLIENT_SECRET, clientSecret);
		map.put(Constants.Param_TOKEN, requestVerify.getToken());

		log.info("request is {} ",map.toString());
		String params = map
				.entrySet().stream()
				.map(entry -> Stream
						.of(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8),
								URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8))
						.collect(Collectors.joining("=")))
				.collect(Collectors.joining("&"));

		return keycloakClient.verifyUserToken(params);

	}

	public Uni<ResponseVerification> validateToken(RequestVerify requestVerify) {
		log.info("request to verify token : {}", requestVerify);
		return this.verifyToken(requestVerify).map(responseAuth -> new ResponseVerification("200","Provided token is correct"))
				.onFailure(throwable -> Optional.ofNullable(throwable)
				.filter(t -> t instanceof WebApplicationException)
				.map(WebApplicationException.class::cast).map(WebApplicationException::getResponse)
				.filter(response -> response.getStatus() == Status.UNAUTHORIZED.getStatusCode())
				.isPresent())
				.transform(t -> {
					return new InternalServerErrorException(InternalServerErrorCodes.ACCESS_TOKEN_INVALID,
							null);

				});

	}



	public Uni<ResponseAuth> checkAndGetAccessToken(HttpHeaders headers, RequestAuth requestAuth) {


		log.info("request to get token : {}", requestAuth);
		AuthenticationService authService = this;
		return authService.getAdminAccessToken().call(responseAdminToken -> {
			String accessToken = "Bearer " + responseAdminToken.getAccessToken();
			log.info(" Admin token :  {} ", accessToken);
			return authService.isExistedUser(accessToken, requestAuth.getUsername()).chain(user -> {

						log.info(" User found :  {} ", user);
						return Uni.createFrom().item(Boolean.TRUE);

					})
					.chain(value -> authService.getUserAccessToken(responseAdminToken.getAccessToken(), requestAuth)
							.chain(responseAccessToken -> {
								log.info(" Access token :  {} ", responseAccessToken.getAccessToken());
								return Uni.createFrom().item(responseAccessToken);

							}).onFailure(throwable -> Optional.ofNullable(throwable)
									.filter(t -> t instanceof WebApplicationException)
									.map(WebApplicationException.class::cast).map(WebApplicationException::getResponse)
									.filter(response -> response.getStatus() == Status.UNAUTHORIZED.getStatusCode())
									.isPresent())
							.transform(t -> {
								return new InternalServerErrorException(InternalServerErrorCodes.USER_CREDENTIAL_EXCEPTION,
										null);

							}));

		});


	}


}
