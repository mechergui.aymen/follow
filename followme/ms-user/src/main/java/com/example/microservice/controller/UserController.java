package com.example.microservice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.microservice.config.KafkaProducer;
import com.example.microservice.exception.ResourceNotFoundException;
import com.example.microservice.model.User;
import com.example.microservice.repository.UserRepository;
import com.example.microservice.service.IUserService;
import com.example.microservice.service.SequenceGeneratorService;


@RefreshScope
@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    IUserService userService;
    
    @Autowired
    KafkaProducer kafkaProducer;
    @GetMapping()
    public List < User > getAllUser(@RequestParam(required = false) String login) throws ResourceNotFoundException{
    	if (login != null && login.length() != 0) {
            List<User> users = userRepository.findByLogin(login);
            if (users.isEmpty()) {
                throw new ResourceNotFoundException("User not found for this last name :: " + login);
            }
            return users;
    	}
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity < User > getEmployeeById(@PathVariable(value = "id") Long iduser)
    throws ResourceNotFoundException {    	
        User user = userRepository.findById(iduser).orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + iduser));
        return ResponseEntity.ok().body(user);
    }
    
  
    @PostMapping()
    public User createEmployee(@Valid @RequestBody User user) {
        user.setId(sequenceGeneratorService.generateSequence(User.SEQUENCE_NAME));
        kafkaProducer.sendUserEvent("add-User", user);
        return userRepository.save(user);
    }

    @PutMapping("/{id}")
    public ResponseEntity < User > updateEmployee(@PathVariable(value = "id") Long iduser,
        @Valid @RequestBody User userDetails) throws ResourceNotFoundException {
        User user = userRepository.findById(iduser)
            .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + iduser));

        user.setName(userDetails.getName());
        user.setLogin(userDetails.getLogin());
        user.setPasssword(userDetails.getPasssword());

        final User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/{id}")
    public Map < String, Boolean > deleteUser(@PathVariable(value = "id") Long iduser)
    throws ResourceNotFoundException {
        User user = userRepository.findById(iduser)
            .orElseThrow(() -> new ResourceNotFoundException("user not found for this id :: " + iduser));

        userRepository.delete(user);
        kafkaProducer.sendUserEvent("delete-user", user);
        Map < String, Boolean > response = new HashMap < > ();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}