package com.example.microservice.model;

import java.beans.JavaBean;

import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
public class UserSerializer implements Serializer<User> {

	    @Override
	    public byte[] serialize(String topic, User data) {
	        try {
	            return new ObjectMapper().writeValueAsBytes(data);
	        } catch (JsonProcessingException e) {
	            throw new RuntimeException(e);
	        }
	    }
	}


