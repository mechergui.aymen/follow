package com.example.microservice.repository;
import  com.example.microservice.model.User;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, Long>{
	List<User> findByLogin(String login);

}