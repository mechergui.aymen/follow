package com.example.microservice.service;

import com.example.microservice.model.User;

public interface IUserService {
	
    public User retrieveUser(long  iduser);

    public void removeUser(long iduser);

}
