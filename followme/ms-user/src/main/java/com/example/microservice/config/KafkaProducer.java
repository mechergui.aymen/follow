package com.example.microservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

import com.example.microservice.model.User;

@Configuration
public class KafkaProducer {


	@Autowired
	private KafkaTemplate<String, User> kafkaTemplate;
	
	
	public void sendUserEvent(String event,User user) {
	    kafkaTemplate.send(event, user);
	}

}