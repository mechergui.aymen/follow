package com.example.microservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.microservice.model.User;
import com.example.microservice.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService implements IUserService {
	@Autowired
	UserRepository userRepository;
	
	public User retrieveUser(long  iduser){
		return userRepository.findById(iduser).get();
	}
	
	public void removeUser(long iduser){
	User e=retrieveUser(iduser);
	userRepository.delete(e);
	}

}