import './App.css'
import Login from './pages/Login'
import Register from './pages/Register'
import Home from './pages/Home'
import NotFound from './pages/NotFound'
import Profile from './pages/Profile'

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Feed from './pages/Feed'
import Users from './pages/Users'

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    errorElement: <NotFound />
  },
  {
    path: "/login",
    element: <Login />,
    errorElement: <NotFound />
  },
  {
    path: "/register",
    element: <Register />,
    errorElement: <NotFound />
  },
  {
    path: "/profile",
    element: <Profile />,
    errorElement: <NotFound />
  }
  ,
  {
    path: "/feed",
    element: <Feed />,
    errorElement: <NotFound />
  },
  {
    path: "/users",
    element: <Users />,
    errorElement: <NotFound />
  },
]);

function App() {

  return (
    <div className="App">
      <RouterProvider router={router}/>
    </div>
  )
}

export default App
