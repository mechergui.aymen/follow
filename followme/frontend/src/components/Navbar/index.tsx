import { NavLink } from "react-router-dom";
import "./navbar.css";
import Button from "../ui/Button";

const index = () => {
  return (
    <nav className="navbar">
      <ul>
        <li>
          <NavLink to="/profile">profile</NavLink>
        </li>
        <li>
          <NavLink to="/feed">feed</NavLink>
        </li>
        <li>
          <NavLink to="/users">users</NavLink>
        </li>
      </ul>
      <button>
        <NavLink to="/login" className="nav-logout">logout</NavLink>
      </button>
    </nav>
  );
};

export default index;
