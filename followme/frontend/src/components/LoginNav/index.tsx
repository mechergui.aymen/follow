import React from "react";
import { NavLink } from "react-router-dom";
import './loginNav.css';

const index = () => {
  return (
    <nav className="login-nav">
      <ul>
        <li>
          <NavLink to="/login">login</NavLink>
        </li>
        <li>
          <NavLink to="/register">register</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default index;
