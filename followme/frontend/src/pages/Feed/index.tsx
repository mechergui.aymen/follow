import "./feed.css";
import Navbar from "../../components/Navbar";

const index: React.FC = () => {
  return (
    <section className="feed">
      <Navbar />
    </section>
  );
};

export default index;
