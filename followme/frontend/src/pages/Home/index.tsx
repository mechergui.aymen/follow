import { NavLink } from "react-router-dom";
import "./home.css";

const index: React.FC = () => {
  return (
    <section className="home">
      <div className="home-content_container">
        <h1>Welcome to mini-social</h1>
        <NavLink to="/login">
          <button>Start</button>
        </NavLink>
      </div>
    </section>
  );
};

export default index;
