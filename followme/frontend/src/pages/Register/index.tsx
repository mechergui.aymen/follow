import "./register.css";
import RegisterForm from "./../../components/RegisterForm";
import LoginNav from "./../../components/LoginNav";

const index: React.FC = () => {
  return (
    <main className="register-page">
      <section>
        <header>
          <LoginNav />
        </header>
        <section className="register-form-wrapper">
          <h1>Welcome !</h1>
          <RegisterForm />
        </section>
      </section>
      <figure>
        <img src="/images/bg.jpg" alt="mountain bg" />
      </figure>
    </main>
  );
};

export default index;
