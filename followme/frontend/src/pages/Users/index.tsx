import "./users.css";
import Navbar from "../../components/Navbar";

const index: React.FC = () => {
  return (
    <section className="users">
      <Navbar />
    </section>
  );
};

export default index;
