import "./profile.css";
import Navbar from "../../components/Navbar";

const index: React.FC = () => {
  return (
    <section className="profile">
      <Navbar />
    </section>
  );
};

export default index;
