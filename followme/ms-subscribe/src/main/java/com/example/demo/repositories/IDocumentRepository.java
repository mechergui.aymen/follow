package com.example.demo.repositories;

import com.example.demo.entities.Subscribe;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface IDocumentRepository extends MongoRepository<Subscribe, Long> {

    Subscribe findByIdUser(Long userId);

}
