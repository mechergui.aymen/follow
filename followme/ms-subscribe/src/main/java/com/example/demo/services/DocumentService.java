package com.example.demo.services;

import com.example.demo.entities.Subscribe;
import com.example.demo.repositories.IDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentService implements IDocumentService {

    @Autowired
    IDocumentRepository iDocumentRepository;

    @Override
    public Subscribe addDocument(Subscribe document) {
        return iDocumentRepository.save(document);
    }

    @Override
    public Subscribe updateDocument(Subscribe document) {
        return  iDocumentRepository.save(document);
    }

    @Override
    public Subscribe getDocumentByUserId(Long userId) {
        Subscribe subscribe=iDocumentRepository.findByIdUser(userId);
        return subscribe;
    }
}
