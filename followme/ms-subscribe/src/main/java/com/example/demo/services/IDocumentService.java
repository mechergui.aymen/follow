package com.example.demo.services;

import com.example.demo.entities.Subscribe;

public interface IDocumentService {

    public Subscribe addDocument(Subscribe document);
    public Subscribe updateDocument(Subscribe document);
    public Subscribe getDocumentByUserId(Long userId);

}
