package com.example.demo.controllers;

import com.example.demo.entities.Subscribe;
import com.example.demo.services.IDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
@RefreshScope
@RestController
@RequestMapping("/api/subscribe")
public class SubscribeController {
  @Value("${yParam}")
  private int yParam;
  @Value("${me}")
  private String me;

  @Autowired
  IDocumentService iDocumentService;


  @PostMapping("/addDocument")
  public Subscribe addDocument(@RequestBody Subscribe subscribe){
    return iDocumentService.addDocument(subscribe);
  }
  @PostMapping("/updateDocumen")
  public Subscribe updateDocument(@RequestBody Subscribe subscribe){
    return iDocumentService.updateDocument(subscribe);
  }
  @GetMapping("/findDocument/{userId}")
  public Subscribe findDocumentById(@PathVariable("userId") Long userId){
    return iDocumentService.getDocumentByUserId(userId);
  }

  @GetMapping("/myConfig")
  public Map<String,Object> myConfig(){
    Map<String,Object> stringObjectMap=new HashMap<>();
    stringObjectMap.put("yParam",yParam);
    stringObjectMap.put("me",me);
    stringObjectMap.put("threadName",Thread.currentThread().getName());
    return  stringObjectMap;
  }
}
