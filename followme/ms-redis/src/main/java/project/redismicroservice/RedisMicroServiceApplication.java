package project.redismicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class RedisMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisMicroServiceApplication.class, args);
	}

}
