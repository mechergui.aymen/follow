package controllers;

import Services.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {

    private RedisService redisService;

    @Autowired
    public MyController(RedisService redisService) {
        this.redisService = redisService;
    }

    @PostMapping("/set")
    public void set(@RequestParam String key, @RequestBody Object value) {
        redisService.set(key, value);
    }

    @GetMapping("/get")
    public Object get(@RequestParam String key) {
        return redisService.get(key);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam String key) {
        redisService.delete(key);
    }
}
