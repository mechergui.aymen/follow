const fileController = require("../controllers/fileController.js");

class Routes {
  constructor(app){
    this.app = app;
  }

  mount(){
    this.status();
    this.uploadFile();
    this.getFileByFileName();
    this.getFilesByUserId();
  }

  status() {
    const PATH = "/api/v1";
    this.app.get(PATH, (req, res, next) => {
      res.status(200).json({
        status: "success",
        message: "OK",
      });
    });
  }

  uploadFile() {
    const PATH = "/api/v1/images";
    this.app.post(
      PATH,
      fileController.uploadFile,
      fileController.resizePhoto
    );
  }

  getFilesByUserId() {
    const PATH = "/api/v1/images";
    this.app.get(PATH, fileController.getFilesByUserId);
  }

  getFileByFileName() {
    const PATH = "/api/v1/images/:imageName";
    this.app.get(PATH, fileController.getFileByFileName);
  }

}

module.exports = Routes;