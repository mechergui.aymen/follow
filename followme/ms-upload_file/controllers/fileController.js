const upload = require("../utils/uploadFile");
const path = require("path");
const sharp = require("sharp");
const fs = require("fs");
const AppError = require("./../utils/appError");
const println = require("./../utils/println");
const FileModel = require("./../models/fileModel");

// Upload image using the photo parameter
const uploadFile = upload.single("photo");

// Resizing user photo using the sharp package
const resizePhoto = async (req, res, next) => {
  if (!req.file || !req.body.user_id) {
    println({service: "uploadFile", level:"error", message: "invalid request parameters (file or user_id)"});
    return next(new AppError("Please provide a valid params!", 400));
  }
  
  try {
    req.file.filename = `user-${req.body.user_id}-${Date.now()}.jpeg`;
    await sharp(req.file.buffer)
    //.resize(500, 500)
    .toFormat("jpeg")
    .jpeg({ quality: 90 })
    .toFile(`public/images/${req.file.filename}`);
  } catch(err){
    println({service: "uploadFile", level:"error", message: "invalid sharp parameters"});
    return next(new AppError("Failed to upload file, check the documentation", 400));
  }
  
  try {
    const userId = req.body.user_id;
    const retrievedDoc = await FileModel.findOne({userId: userId});

    if(!retrievedDoc){
      const file = new FileModel();
      const imageName = req.file.filename.substring(0, (req.file.filename.length - 5));

      file.userId = userId;
      file.images.push(imageName);

      const newlyCreatedDoc = await file.save();
      println({service: "uploadFile", level:"info", message: "Image has been uploaded, document updated successfuly"});
      res.status(201).json({
        status: "success",
        data: newlyCreatedDoc,
      });
    } else {
      const imageName = req.file.filename.substring(0, (req.file.filename.length - 5));
      retrievedDoc.images.push(imageName);
      const updatedDoc = await retrievedDoc.save();
      println({service: "uploadFile", level:"info", message: "Image has been uploaded, document created successfuly"});
      res.status(200).json({
        status: "success",
        data: updatedDoc,
      });
    }
  } catch(err){
    println({service: "uploadFile", level:"error", message: "something when wrong when saving or updating model"});
    return next(new AppError("Error while saving data!", 400));
  }
};

// Return user image
const getFileByFileName = (req, res, next) => {
  const fileName = `${req.params.imageName}.jpeg`;
  const filePath = path.resolve(__dirname,"..", "public","images", fileName);
  fs.access(filePath, fs.F_OK, (err) => {
    if (err) {
      println({service: "getFile", level:"info", message: `file not found with name ${fileName}`});
      return next(new AppError("file not found", 404));
    } else { 
      println({service: "getFile", level:"info", message: "File has been found, responding with file"});
      res.sendFile(filePath);
    }
  })
};

// Return user image
const getFilesByUserId = async (req, res, next) => {
  const userId = req.query?.user_id;
  if(!userId){
    println({service: "uploadFile", level:"error", message: "Missing query string"});
    res.status(400).json({
      status: "fail",
      message: "Missing query string!",
    });
  } else {
    try {
      const retrievedDoc = await FileModel.findOne({userId: userId});
      if(!retrievedDoc){
        return res.status(200).json({
          status: "success",
          message: `No document found with id ${userId}`,
        });
      }
      return res.status(200).json({
        status: "success",
        data: retrievedDoc,
      });
    } catch (error) {
      println({service: "uploadFile", level:"error", message: "Error while searching for doc"});
      return next(new AppError("Error when finding document", 400));
    }
  }
};

module.exports = {
  uploadFile,
  resizePhoto,
  getFileByFileName,
  getFilesByUserId
};
