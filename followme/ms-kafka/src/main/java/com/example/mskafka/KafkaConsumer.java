package com.example.mskafka;

import org.slf4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.mskafka.entities.User;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class KafkaConsumer {

	    @KafkaListener(topics = "add-User",
        groupId = "group_id")
	    public void listen(User user) {
	    	  System.out.print( "KAFKA CONSUMER...");
	      System.out.print( user);
	    }
	

}
