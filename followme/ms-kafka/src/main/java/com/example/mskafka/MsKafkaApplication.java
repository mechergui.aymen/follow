package com.example.mskafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;



@EnableEurekaClient
@SpringBootApplication
public class MsKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsKafkaApplication.class, args);
	}

}
