package com.example.mskafka.entities;



import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "User")
public class User {

     public static final String SEQUENCE_NAME = "users_sequence";

    private long id;


    private String name;
    

    private String login;
    

    private String passsword;
}