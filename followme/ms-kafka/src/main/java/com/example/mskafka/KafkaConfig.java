package com.example.mskafka;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import com.example.mskafka.entities.User;
import com.fasterxml.jackson.databind.JsonDeserializer;

@EnableKafka
@Configuration
public class KafkaConfig {

	  
	    @Bean
	    public ConsumerFactory<String, User> consumerFactory()
	    {
	  
	     
	        Map<String, Object> map = new HashMap<>();
	  
	        map.put(ConsumerConfig
                    .GROUP_ID_CONFIG,
                "id");
        map.put(ConsumerConfig
                    .KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        map.put(ConsumerConfig
                    .VALUE_DESERIALIZER_CLASS_CONFIG,
                JsonDeserializer.class);
	  
	        return new DefaultKafkaConsumerFactory<>(map);
	    }
	  
	 
	    @Bean
	    public ConcurrentKafkaListenerContainerFactory<String,
	                                                   User>
	    studentListner()
	    {
	        ConcurrentKafkaListenerContainerFactory<String,
	                                                User>
	            factory
	            = new ConcurrentKafkaListenerContainerFactory<>();
	        factory.setConsumerFactory(consumerFactory());
	        return factory;
	    }
	}

