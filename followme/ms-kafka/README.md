# Kafka Microservice

- This microservice is responsible for handling communication between two other microservices - NewsFeeds and Subscribe - using Kafka.

# Technologies

- Java 11
- Spring Boot 2.7.10
- Apache Kafka 2.13
- Docker 20.10.7


# Configuration

- The Kafka brokers can be configured in application.properties.
- The topic names can also be configured in application.properties.

# Usage
- To start the microservice, run the Docker image: docker run -p 8787:8787 ms-kafka
