# Architecture Overview

The architecture consists of the following components:

- React Front-End
- Spring Cloud Gateway
- Eureka Server
- Config Server
- Kafka
- Keycloak
- Redis

![Architecture Diagram](followme/images/architecture.drawio.png)

## Component Roles

1. **React Front-End:** This component provides the user interface for the application. It sends requests to the Spring Cloud Gateway.

2. **Spring Cloud Gateway:** This component acts as the entry point to the microservices. It routes requests from the front-end to the appropriate microservices based on the URL and other criteria.

3. **Eureka Server:** This component is a service registry that keeps track of all the microservices registered with it. It allows the Spring Cloud Gateway to discover the location of the microservices.

4. **Config Server:** This component stores the configuration details of all the microservices. Each microservice can request its configuration from the Config Server during startup.

5. **Kafka:** This component is a distributed messaging system that enables real-time data synchronization between the user microservice and the Keycloak service.

6. **Keycloak:** This component is an open-source identity and access management solution. It handles authentication and authorization for the microservices.

7. **Redis:**  to be continued  @Salim 

Overall, this architecture is designed to enable scalability, reliability, and flexibility for the application. The front-end sends requests to the Spring Cloud Gateway, which routes them to the appropriate microservices. The Eureka Server helps the gateway locate the microservices, while the Config Server provides the configuration details. Kafka enables real-time data synchronization between the user microservice and Keycloak, while Keycloak handles authentication and authorization for the microservices.
